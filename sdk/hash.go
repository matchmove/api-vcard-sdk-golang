package sdk

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io"
)

// Hash represents a Hashable string
type Hash struct {
	S string
}

// MD5 calculates the MD5 hash of a string
func (h Hash) MD5() string {
	m := md5.New()
	io.WriteString(m, h.S)
	return fmt.Sprintf("%x", m.Sum(nil))
}

// SHA1 calculates the SHA1 hash of a string
func (h Hash) SHA1() string {
	m := sha1.New()
	io.WriteString(m, h.S)
	return fmt.Sprintf("%x", m.Sum(nil))
}

// HMACSHA1 produces an HMAC-SHA1 string hash
func (h Hash) HMACSHA1(key string) string {
	m := hmac.New(sha1.New, []byte(key))
	m.Write([]byte(h.S))
	return base64.StdEncoding.EncodeToString(m.Sum(nil))
}
