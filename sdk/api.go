package sdk

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// API contains all the values needed to consume any endpoint in the API
type API struct {
	Host     string
	Resource string
	Method   string
	Params   map[string]string
	Creds    Creds
	User     User
	Token    Token
	Response map[string]interface{}
}

// Creds contain API credentials
type Creds struct {
	Key, Secret string
}

// User details used for authenticated transactions
type User struct {
	Login, Password string
}

const (
	signatureMethod = "HMAC-SHA1"
)

// Connection sets required data for any request
func (api *API) Connection(host string, key string, secret string) error {
	if !IsURL(host) {
		return fmt.Errorf("The value for `host` is not a valid URL.")
	}

	if 0 >= len(key) || 0 >= len(secret) {
		return fmt.Errorf("Non-empty values are required to establish a `Connection`.")
	}

	api.Host = host
	api.Creds.Key = key
	api.Creds.Secret = secret
	return nil
}

// Authenticate uses oauth for access to resources that need
// an authenticated user
func (api *API) Authenticate(user string, password string) error {
	api.User = User{
		Login:    user,
		Password: password,
	}

	if 0 >= len(api.Host) || 0 >= len(api.Creds.Key) || 0 >= len(api.Creds.Secret) {
		return fmt.Errorf("Setting a `Connection` is required for authentication.")
	}

	if err := api.AccessToken(); err != nil {
		return fmt.Errorf("Token request or access error. %s", err)
	}

	return nil
}

// Consume sends a request to the Host and Endpoint using the Method
func (api API) Consume(resource string, method string, query map[string]string) (*http.Response, string, error) {
	api.initParams()
	var strQuery string
	secret := Encode(api.Creds.Secret)

	if 0 < len(api.Token.OAuthToken) {
		api.Params["oauth_token"] = api.Token.OAuthToken
		secret = fmt.Sprintf("%s&%s", Encode(api.Creds.Secret), Encode(api.Token.OAuthTokenSecret))

		if 0 < len(query) {
			for k, v := range query {
				api.Params[k] = v
			}
		}
	} else {

		if 0 < len(query) {
			strQuery = ToURL(query)
			api.Params["_d"] = strQuery
		}
	}

	api.Method = strings.ToUpper(method)
	api.Resource = resource

	signature, err := api.Sign(secret)
	if err != nil {
		return nil, "", fmt.Errorf("Consume error: %s", err)
	}
	api.Params["oauth_signature"] = signature

	if 0 < len(strQuery) {
		log.Printf("ANONYMOUS QUERY: %s", strQuery)
		key := Hash{api.Params["oauth_signature"] + api.Creds.Secret}.MD5()
		e := Encryption{}
		api.Params["_d"], _ = e.EncryptString(key, strQuery)
	}

	response, body := api.Request()
	log.Printf("HTTP RESPONSE: %#v", response)

	return response, body, nil
}

// Request sends HTTP requests based on the contents of API
func (api API) Request() (*http.Response, string) {
	var payload string
	if 0 < len(api.Params) {
		payload = ToURL(api.Params)
	}

	method := strings.ToUpper(api.Method)
	URL := fmt.Sprintf("%s/%s", api.Host, api.Resource)

	if method != "POST" {
		URL = fmt.Sprintf("%s?%s", URL, payload)
	}

	log.Printf("REQUEST URL: %s", URL)
	log.Printf("REQUEST METHOD: %s", method)
	log.Printf("REQUEST PAYLOAD: %s", payload)

	request, err := http.NewRequest(
		method,
		URL,
		strings.NewReader(payload),
	)

	if nil != err {
		log.Panicf("API Request encountered an error: %s", err)
	}

	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	response, err := http.DefaultClient.Do(request)

	if nil != err {
		log.Panicf("API Request encountered an error while retrieving response: %s", err)
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	if nil != err {
		log.Fatalf("API Request encoutered an error while reading the response: %s", err)
	}

	return response, string(body)
}

// Sign generates an Oauth signature
func (api API) Sign(secret string) (string, error) {
	if 0 >= len(api.Method) || 0 >= len(api.Resource) {
		return "", fmt.Errorf("API Method or Resource is not set")
	}

	u := api.Host + "/" + api.Resource
	s := fmt.Sprintf("%s&%s&%s", Encode(api.Method), Encode(u), Encode(ToURL(api.Params)))
	h := Hash{s}.HMACSHA1(secret)

	log.Printf("PLAIN SIGNATURE: %s", s)
	log.Printf("HASHED SIGNATURE: %s", h)

	return h, nil
}

// InitParams return new values for auto-generated values
func (api *API) initParams() {
	for k := range api.Params {
		delete(api.Params, k)
	}

	api.Params = map[string]string{
		"oauth_consumer_key":     api.Creds.Key,
		"oauth_nonce":            GetNonce(),
		"oauth_signature_method": signatureMethod,
		"oauth_timestamp":        GetTimestamp(),
		"oauth_version":          oAuthVersion,
	}
}
