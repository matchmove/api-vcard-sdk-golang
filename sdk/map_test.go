// +build all maps

package sdk

import "testing"

func TestToURL(t *testing.T) {
	m := map[string]string{
		"testParam1": "testValue1",
		"testParam2": "testValue2",
	}

	URLStr := ToURL(m)
	if URLStr != "testParam1=testValue1&testParam2=testValue2" {
		t.Errorf("Incorrect conversion of map to URL string in: %s", "ToURL")
	}
}

func TestEncode(t *testing.T) {
	want := "Test%3Dunescaped%26string%20"
	d := "Test=unescaped&string "
	got := Encode(d)

	if want != got {
		t.Errorf("TestEncode returned unexpected results. Got: %s, Want: %s", got, want)
	}
}

func TestSortMapByKeys(t *testing.T) {
	want := [4]string{"a", "b", "c", "d"}
	d := map[string]string{"c": "c", "a": "a", "d": "d", "b": "b"}
	gotKey, gotVal := SortMapByKey(d)

	for i := 0; i < len(d); i++ {
		if want[i] != gotKey[i] || want[i] != gotVal[i] {
			t.Errorf("TestSortMapByKeys returned unexpected results. Got: key %v => %v, Want: %v => %v",
				gotKey, want, gotVal, want)
		}
	}
}
